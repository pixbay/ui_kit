#!/bin/sh

ls &&
fvm flutter pub add freezed
fvm flutter pub add mason
fvm flutter pub add golden_toolkit
fvm flutter pub add flutter_svg
fvm flutter pub add build_runner
fvm flutter pub add cached_network_image
fvm flutter pub add json_annotation

fvm flutter pub get

mason init
mason upgrade
mason add ui_component --path gen/bricks/ui_component
