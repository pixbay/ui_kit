import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class {{classname.pascalCase()}}Model with _${{classname.pascalCase()}}Model {
  const factory {{classname.pascalCase()}}Model({
    required String value,
  }) = _{{classname.pascalCase()}}Model;
}
