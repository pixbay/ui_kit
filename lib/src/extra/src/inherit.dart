import 'package:flutter/widgets.dart';

class SessionInheritedWidget extends InheritedWidget {
  final String? sessionToken;

  const SessionInheritedWidget({
    required super.child,
    this.sessionToken,
    super.key,
  });

  static SessionInheritedWidget? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<SessionInheritedWidget>();
  }

  @override
  bool updateShouldNotify(SessionInheritedWidget oldWidget) => sessionToken != oldWidget.sessionToken;
}
