extension PackagePrefix on String {
  String get packagePrefix {
    return 'packages/ui_kit/$this';
  }
}
