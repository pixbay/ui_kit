import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:ui_kit/src/resources/index.dart';
import 'package:ui_kit/src/resources/src/colors/components/index.dart';

export 'src/colors/components/index.dart';

part 'src/colors/base_tokens/default.dart';
part 'src/icons/icon.dart';
part 'src/metrics.dart';
part 'src/text_styles/default.dart';
part 'src/text_styles/sizes.dart';
part 'src/themes.dart';
