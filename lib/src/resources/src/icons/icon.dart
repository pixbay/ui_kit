part of '../../index.dart';

enum IconType {
  image,
  eyeConturOpen,
  eyeConturClose,
  favorite,
  close,
  search,
}

class IconWidget extends StatelessWidget {
  const IconWidget.size3XS({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.size3XS;

  const IconWidget.size2XS({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.size2XS;

  const IconWidget.sizeXS({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.sizeXS;

  const IconWidget.sizeS({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.sizeS;

  const IconWidget.size2XM({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.size2XM;

  const IconWidget.sizeM({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.sizeM;

  const IconWidget.sizeXL({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.sizeXL;

  const IconWidget.size2XL({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.size2XL;

  const IconWidget.size5XL({
    super.key,
    required this.color,
    required this.type,
  }) : size = Sizing.size5XL;

  final IconType type;
  final double size;
  final Color color;

  @override
  Widget build(BuildContext context) {
    final IconData icon;

    switch (type) {
      case IconType.search:
        icon = Icons.search;
        break;
      case IconType.eyeConturOpen:
        icon = Icons.visibility_outlined;
        break;
      case IconType.eyeConturClose:
        icon = Icons.visibility_off_outlined;
        break;
      case IconType.image:
        icon = Icons.image;
        break;
      case IconType.favorite:
        icon = Icons.favorite;
        break;
      case IconType.close:
        icon = Icons.close;
        break;
    }

    return Icon(
      icon,
      color: color,
      size: size,
    );
  }
}
