part of '../../index.dart';

class ButtonWarningGhostColors extends ThemeExtension<ButtonWarningGhostColors> {
  ButtonWarningGhostColors({
    required this.hoverFill,
    required this.pressedFill,
    required this.focusedBorder2,
    required this.defaultIcon,
    required this.hoverIcon,
    required this.pressedIcon,
    required this.disabledIcon,
    required this.focusedIcon,
    required this.loadingIcon,
  });

  final Color hoverFill;
  final Color pressedFill;
  final Color focusedBorder2;
  final Color defaultIcon;
  final Color hoverIcon;
  final Color pressedIcon;
  final Color disabledIcon;
  final Color focusedIcon;
  final Color loadingIcon;

  @override
  ButtonWarningGhostColors lerp(ButtonWarningGhostColors? other, double t) {
    if (other is! ButtonWarningGhostColors) {
      return this;
    }

    return ButtonWarningGhostColors(
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      pressedFill: Color.lerp(pressedFill, other.pressedFill, t)!,
      focusedBorder2: Color.lerp(focusedBorder2, other.focusedBorder2, t)!,
      defaultIcon: Color.lerp(defaultIcon, other.defaultIcon, t)!,
      hoverIcon: Color.lerp(hoverIcon, other.hoverIcon, t)!,
      pressedIcon: Color.lerp(pressedIcon, other.pressedIcon, t)!,
      disabledIcon: Color.lerp(disabledIcon, other.disabledIcon, t)!,
      focusedIcon: Color.lerp(focusedIcon, other.focusedIcon, t)!,
      loadingIcon: Color.lerp(loadingIcon, other.loadingIcon, t)!,
    );
  }

  @override
  ButtonWarningGhostColors copyWith({
    Color? hoverFill,
    Color? pressedFill,
    Color? focusedBorder2,
    Color? defaultIcon,
    Color? hoverIcon,
    Color? pressedIcon,
    Color? disabledIcon,
    Color? focusedIcon,
    Color? loadingIcon,
  }) {
    return ButtonWarningGhostColors(
      hoverFill: hoverFill ?? this.hoverFill,
      pressedFill: pressedFill ?? this.pressedFill,
      focusedBorder2: focusedBorder2 ?? this.focusedBorder2,
      defaultIcon: defaultIcon ?? this.defaultIcon,
      hoverIcon: hoverIcon ?? this.hoverIcon,
      pressedIcon: pressedIcon ?? this.pressedIcon,
      disabledIcon: disabledIcon ?? this.disabledIcon,
      focusedIcon: focusedIcon ?? this.focusedIcon,
      loadingIcon: loadingIcon ?? this.loadingIcon,
    );
  }
}
