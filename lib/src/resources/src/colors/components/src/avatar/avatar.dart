part of '../../index.dart';

class AvatarColors extends ThemeExtension<AvatarColors> {
  AvatarColors({
    required this.dodgerFill,
    required this.purpleFill,
    required this.redFill,
    required this.orangeFill,
    required this.greenFill,
    required this.moreFill,
    required this.iconFill,
    required this.dodgerBorder,
    required this.purpleBorder,
    required this.redBorder,
    required this.orangeBorder,
    required this.greenBorder,
    required this.moreBorder,
    required this.iconBorder,
    required this.imageBorder,
    required this.dodgerText,
    required this.purpleText,
    required this.redText,
    required this.orangeText,
    required this.greenText,
    required this.moreText,
    required this.iconIcon,
  });

  final Color dodgerFill;
  final Color purpleFill;
  final Color redFill;
  final Color orangeFill;
  final Color greenFill;
  final Color moreFill;
  final Color iconFill;
  final Color dodgerBorder;
  final Color purpleBorder;
  final Color redBorder;
  final Color orangeBorder;
  final Color greenBorder;
  final Color moreBorder;
  final Color iconBorder;
  final Color imageBorder;
  final Color dodgerText;
  final Color purpleText;
  final Color redText;
  final Color orangeText;
  final Color greenText;
  final Color moreText;
  final Color iconIcon;

  @override
  AvatarColors lerp(AvatarColors? other, double t) {
    if (other is! AvatarColors) {
      return this;
    }

    return AvatarColors(
      dodgerFill: Color.lerp(dodgerFill, other.dodgerFill, t)!,
      purpleFill: Color.lerp(purpleFill, other.purpleFill, t)!,
      redFill: Color.lerp(redFill, other.redFill, t)!,
      orangeFill: Color.lerp(orangeFill, other.orangeFill, t)!,
      greenFill: Color.lerp(greenFill, other.greenFill, t)!,
      moreFill: Color.lerp(moreFill, other.moreFill, t)!,
      iconFill: Color.lerp(iconFill, other.iconFill, t)!,
      dodgerBorder: Color.lerp(dodgerBorder, other.dodgerBorder, t)!,
      purpleBorder: Color.lerp(purpleBorder, other.purpleBorder, t)!,
      redBorder: Color.lerp(redBorder, other.redBorder, t)!,
      orangeBorder: Color.lerp(orangeBorder, other.orangeBorder, t)!,
      greenBorder: Color.lerp(greenBorder, other.greenBorder, t)!,
      moreBorder: Color.lerp(moreBorder, other.moreBorder, t)!,
      iconBorder: Color.lerp(iconBorder, other.iconBorder, t)!,
      imageBorder: Color.lerp(imageBorder, other.imageBorder, t)!,
      dodgerText: Color.lerp(dodgerText, other.dodgerText, t)!,
      purpleText: Color.lerp(purpleText, other.purpleText, t)!,
      redText: Color.lerp(redText, other.redText, t)!,
      orangeText: Color.lerp(orangeText, other.orangeText, t)!,
      greenText: Color.lerp(greenText, other.greenText, t)!,
      moreText: Color.lerp(moreText, other.moreText, t)!,
      iconIcon: Color.lerp(iconIcon, other.iconIcon, t)!,
    );
  }

  @override
  AvatarColors copyWith({
    Color? dodgerFill,
    Color? purpleFill,
    Color? redFill,
    Color? orangeFill,
    Color? greenFill,
    Color? moreFill,
    Color? iconFill,
    Color? dodgerBorder,
    Color? purpleBorder,
    Color? redBorder,
    Color? orangeBorder,
    Color? greenBorder,
    Color? moreBorder,
    Color? iconBorder,
    Color? imageBorder,
    Color? dodgerText,
    Color? purpleText,
    Color? redText,
    Color? orangeText,
    Color? greenText,
    Color? moreText,
    Color? iconIcon,
  }) {
    return AvatarColors(
      dodgerFill: dodgerFill ?? this.dodgerFill,
      purpleFill: purpleFill ?? this.purpleFill,
      redFill: redFill ?? this.redFill,
      orangeFill: orangeFill ?? this.orangeFill,
      greenFill: greenFill ?? this.greenFill,
      moreFill: moreFill ?? this.moreFill,
      iconFill: iconFill ?? this.iconFill,
      dodgerBorder: dodgerBorder ?? this.dodgerBorder,
      purpleBorder: purpleBorder ?? this.purpleBorder,
      redBorder: redBorder ?? this.redBorder,
      orangeBorder: orangeBorder ?? this.orangeBorder,
      greenBorder: greenBorder ?? this.greenBorder,
      moreBorder: moreBorder ?? this.moreBorder,
      iconBorder: iconBorder ?? this.iconBorder,
      imageBorder: imageBorder ?? this.imageBorder,
      dodgerText: dodgerText ?? this.dodgerText,
      purpleText: purpleText ?? this.purpleText,
      redText: redText ?? this.redText,
      orangeText: orangeText ?? this.orangeText,
      greenText: greenText ?? this.greenText,
      moreText: moreText ?? this.moreText,
      iconIcon: iconIcon ?? this.iconIcon,
    );
  }
}
