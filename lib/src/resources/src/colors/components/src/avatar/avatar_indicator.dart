part of '../../index.dart';

class AvatarIndicatorColors extends ThemeExtension<AvatarIndicatorColors> {
  AvatarIndicatorColors({
    required this.availableFill,
    required this.awayFill,
    required this.offFill,
    required this.availableBorder,
    required this.awayBorder,
    required this.offBorder,
  });

  final Color availableFill;
  final Color awayFill;
  final Color offFill;
  final Color availableBorder;
  final Color awayBorder;
  final Color offBorder;

  @override
  AvatarIndicatorColors lerp(AvatarIndicatorColors? other, double t) {
    if (other is! AvatarIndicatorColors) {
      return this;
    }

    return AvatarIndicatorColors(
      availableFill: Color.lerp(availableFill, other.availableFill, t)!,
      awayFill: Color.lerp(awayFill, other.awayFill, t)!,
      offFill: Color.lerp(offFill, other.offFill, t)!,
      availableBorder: Color.lerp(availableBorder, other.availableBorder, t)!,
      awayBorder: Color.lerp(awayBorder, other.awayBorder, t)!,
      offBorder: Color.lerp(offBorder, other.offBorder, t)!,
    );
  }

  @override
  AvatarIndicatorColors copyWith({
    Color? availableFill,
    Color? awayFill,
    Color? offFill,
    Color? availableBorder,
    Color? awayBorder,
    Color? offBorder,
  }) {
    return AvatarIndicatorColors(
      availableFill: availableFill ?? this.availableFill,
      awayFill: awayFill ?? this.awayFill,
      offFill: offFill ?? this.offFill,
      availableBorder: availableBorder ?? this.availableBorder,
      awayBorder: awayBorder ?? this.awayBorder,
      offBorder: offBorder ?? this.offBorder,
    );
  }
}
