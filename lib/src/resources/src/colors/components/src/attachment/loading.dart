part of '../../index.dart';

class AttachmentLoadingColors extends ThemeExtension<AttachmentLoadingColors> {
  AttachmentLoadingColors({
    required this.uploadingFill,
    required this.defaultFill,
    required this.hoverFill,
    required this.focusedFill,
    required this.errorFill,
    required this.downloadingFill,
    required this.uploadingBorder,
    required this.defaultBorder,
    required this.hoverBorder,
    required this.focusedBorder,
    required this.errorBorder,
    required this.downloadingBorder,
    required this.uploadingText,
    required this.defaultText,
    required this.hoverText,
    required this.focusedText,
    required this.errorText,
    required this.downloadingText,
  });

  final Color uploadingFill;
  final Color defaultFill;
  final Color hoverFill;
  final Color focusedFill;
  final Color errorFill;
  final Color downloadingFill;
  final Color uploadingBorder;
  final Color defaultBorder;
  final Color hoverBorder;
  final Color focusedBorder;
  final Color errorBorder;
  final Color downloadingBorder;
  final Color uploadingText;
  final Color defaultText;
  final Color hoverText;
  final Color focusedText;
  final Color errorText;
  final Color downloadingText;

  @override
  AttachmentLoadingColors lerp(AttachmentLoadingColors? other, double t) {
    if (other is! AttachmentLoadingColors) {
      return this;
    }

    return AttachmentLoadingColors(
      uploadingFill: Color.lerp(uploadingFill, other.uploadingFill, t)!,
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      focusedFill: Color.lerp(focusedFill, other.focusedFill, t)!,
      errorFill: Color.lerp(errorFill, other.errorFill, t)!,
      downloadingFill: Color.lerp(downloadingFill, other.downloadingFill, t)!,
      uploadingBorder: Color.lerp(uploadingBorder, other.uploadingBorder, t)!,
      defaultBorder: Color.lerp(defaultBorder, other.defaultBorder, t)!,
      hoverBorder: Color.lerp(hoverBorder, other.hoverBorder, t)!,
      focusedBorder: Color.lerp(focusedBorder, other.focusedBorder, t)!,
      errorBorder: Color.lerp(errorBorder, other.errorBorder, t)!,
      downloadingBorder:
          Color.lerp(downloadingBorder, other.downloadingBorder, t)!,
      uploadingText: Color.lerp(uploadingText, other.uploadingText, t)!,
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
      hoverText: Color.lerp(hoverText, other.hoverText, t)!,
      focusedText: Color.lerp(focusedText, other.focusedText, t)!,
      errorText: Color.lerp(errorText, other.errorText, t)!,
      downloadingText: Color.lerp(downloadingText, other.downloadingText, t)!,
    );
  }

  @override
  AttachmentLoadingColors copyWith({
    Color? uploadingFill,
    Color? defaultFill,
    Color? hoverFill,
    Color? focusedFill,
    Color? errorFill,
    Color? downloadingFill,
    Color? uploadingBorder,
    Color? defaultBorder,
    Color? hoverBorder,
    Color? focusedBorder,
    Color? errorBorder,
    Color? downloadingBorder,
    Color? uploadingText,
    Color? defaultText,
    Color? hoverText,
    Color? focusedText,
    Color? errorText,
    Color? downloadingText,
  }) {
    return AttachmentLoadingColors(
      uploadingFill: uploadingFill ?? this.uploadingFill,
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      focusedFill: focusedFill ?? this.focusedFill,
      errorFill: errorFill ?? this.errorFill,
      downloadingFill: downloadingFill ?? this.downloadingFill,
      uploadingBorder: uploadingBorder ?? this.uploadingBorder,
      defaultBorder: defaultBorder ?? this.defaultBorder,
      hoverBorder: hoverBorder ?? this.hoverBorder,
      focusedBorder: focusedBorder ?? this.focusedBorder,
      errorBorder: errorBorder ?? this.errorBorder,
      downloadingBorder: downloadingBorder ?? this.downloadingBorder,
      uploadingText: uploadingText ?? this.uploadingText,
      defaultText: defaultText ?? this.defaultText,
      hoverText: hoverText ?? this.hoverText,
      focusedText: focusedText ?? this.focusedText,
      errorText: errorText ?? this.errorText,
      downloadingText: downloadingText ?? this.downloadingText,
    );
  }
}
