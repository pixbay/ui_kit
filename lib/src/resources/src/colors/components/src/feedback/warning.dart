part of '../../index.dart';

class FeedbackWarningColors extends ThemeExtension<FeedbackWarningColors> {
  FeedbackWarningColors({
    required this.fill,
    required this.additionalFill,
    required this.text,
    required this.additionalText,
    required this.icon,
    required this.additionalIcon,
  });

  final Color fill;
  final Color additionalFill;
  final Color text;
  final Color additionalText;
  final Color icon;
  final Color additionalIcon;

  @override
  FeedbackWarningColors lerp(FeedbackWarningColors? other, double t) {
    if (other is! FeedbackWarningColors) {
      return this;
    }

    return FeedbackWarningColors(
      fill: Color.lerp(fill, other.fill, t)!,
      additionalFill: Color.lerp(additionalFill, other.additionalFill, t)!,
      text: Color.lerp(text, other.text, t)!,
      additionalText: Color.lerp(additionalText, other.additionalText, t)!,
      icon: Color.lerp(icon, other.icon, t)!,
      additionalIcon: Color.lerp(additionalIcon, other.additionalIcon, t)!,
    );
  }

  @override
  FeedbackWarningColors copyWith({
    Color? fill,
    Color? additionalFill,
    Color? text,
    Color? additionalText,
    Color? icon,
    Color? additionalIcon,
  }) {
    return FeedbackWarningColors(
      fill: fill ?? this.fill,
      additionalFill: additionalFill ?? this.additionalFill,
      text: text ?? this.text,
      additionalText: additionalText ?? this.additionalText,
      icon: icon ?? this.icon,
      additionalIcon: additionalIcon ?? this.additionalIcon,
    );
  }
}
