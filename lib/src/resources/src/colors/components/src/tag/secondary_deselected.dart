part of '../../index.dart';

class TagSecondaryDeselectedColors
    extends ThemeExtension<TagSecondaryDeselectedColors> {
  TagSecondaryDeselectedColors({
    required this.defaultFill,
    required this.hoverFill,
    required this.pressedFill,
    required this.disabledFill,
    required this.focusedFill,
    required this.defaultBorder,
    required this.hoverBorder,
    required this.pressedBorder,
    required this.disabledBorder,
    required this.focusedBorder,
    required this.focusedBorder2,
    required this.defaultText,
    required this.hoverText,
    required this.pressedText,
    required this.disabledText,
    required this.focusedText,
    required this.defaultIcon,
    required this.hoverIcon,
    required this.pressedIcon,
    required this.disabledIcon,
    required this.focusedIcon,
  });

  final Color defaultFill;
  final Color hoverFill;
  final Color pressedFill;
  final Color disabledFill;
  final Color focusedFill;
  final Color defaultBorder;
  final Color hoverBorder;
  final Color pressedBorder;
  final Color disabledBorder;
  final Color focusedBorder;
  final Color focusedBorder2;
  final Color defaultText;
  final Color hoverText;
  final Color pressedText;
  final Color disabledText;
  final Color focusedText;
  final Color defaultIcon;
  final Color hoverIcon;
  final Color pressedIcon;
  final Color disabledIcon;
  final Color focusedIcon;

  @override
  TagSecondaryDeselectedColors lerp(
      TagSecondaryDeselectedColors? other, double t) {
    if (other is! TagSecondaryDeselectedColors) {
      return this;
    }

    return TagSecondaryDeselectedColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      pressedFill: Color.lerp(pressedFill, other.pressedFill, t)!,
      disabledFill: Color.lerp(disabledFill, other.disabledFill, t)!,
      focusedFill: Color.lerp(focusedFill, other.focusedFill, t)!,
      defaultBorder: Color.lerp(defaultBorder, other.defaultBorder, t)!,
      hoverBorder: Color.lerp(hoverBorder, other.hoverBorder, t)!,
      pressedBorder: Color.lerp(pressedBorder, other.pressedBorder, t)!,
      disabledBorder: Color.lerp(disabledBorder, other.disabledBorder, t)!,
      focusedBorder: Color.lerp(focusedBorder, other.focusedBorder, t)!,
      focusedBorder2: Color.lerp(focusedBorder2, other.focusedBorder2, t)!,
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
      hoverText: Color.lerp(hoverText, other.hoverText, t)!,
      pressedText: Color.lerp(pressedText, other.pressedText, t)!,
      disabledText: Color.lerp(disabledText, other.disabledText, t)!,
      focusedText: Color.lerp(focusedText, other.focusedText, t)!,
      defaultIcon: Color.lerp(defaultIcon, other.defaultIcon, t)!,
      hoverIcon: Color.lerp(hoverIcon, other.hoverIcon, t)!,
      pressedIcon: Color.lerp(pressedIcon, other.pressedIcon, t)!,
      disabledIcon: Color.lerp(disabledIcon, other.disabledIcon, t)!,
      focusedIcon: Color.lerp(focusedIcon, other.focusedIcon, t)!,
    );
  }

  @override
  TagSecondaryDeselectedColors copyWith({
    Color? defaultFill,
    Color? hoverFill,
    Color? pressedFill,
    Color? disabledFill,
    Color? focusedFill,
    Color? defaultBorder,
    Color? hoverBorder,
    Color? pressedBorder,
    Color? disabledBorder,
    Color? focusedBorder,
    Color? focusedBorder2,
    Color? defaultText,
    Color? hoverText,
    Color? pressedText,
    Color? disabledText,
    Color? focusedText,
    Color? defaultIcon,
    Color? hoverIcon,
    Color? pressedIcon,
    Color? disabledIcon,
    Color? focusedIcon,
  }) {
    return TagSecondaryDeselectedColors(
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      pressedFill: pressedFill ?? this.pressedFill,
      disabledFill: disabledFill ?? this.disabledFill,
      focusedFill: focusedFill ?? this.focusedFill,
      defaultBorder: defaultBorder ?? this.defaultBorder,
      hoverBorder: hoverBorder ?? this.hoverBorder,
      pressedBorder: pressedBorder ?? this.pressedBorder,
      disabledBorder: disabledBorder ?? this.disabledBorder,
      focusedBorder: focusedBorder ?? this.focusedBorder,
      focusedBorder2: focusedBorder2 ?? this.focusedBorder2,
      defaultText: defaultText ?? this.defaultText,
      hoverText: hoverText ?? this.hoverText,
      pressedText: pressedText ?? this.pressedText,
      disabledText: disabledText ?? this.disabledText,
      focusedText: focusedText ?? this.focusedText,
      defaultIcon: defaultIcon ?? this.defaultIcon,
      hoverIcon: hoverIcon ?? this.hoverIcon,
      pressedIcon: pressedIcon ?? this.pressedIcon,
      disabledIcon: disabledIcon ?? this.disabledIcon,
      focusedIcon: focusedIcon ?? this.focusedIcon,
    );
  }
}
