part of '../../index.dart';

class TextFieldFilledColors extends ThemeExtension<TextFieldFilledColors> {
  TextFieldFilledColors({
    required this.defaultFill,
    required this.hoverFill,
    required this.disabledFill,
    required this.focusedFill,
    required this.activatedFill,
    required this.goodFill,
    required this.badFill,
    required this.warningFill,
    required this.disabledBorder,
    required this.activatedBorder,
    required this.goodBorder,
    required this.badBorder,
    required this.warningBorder,
    required this.focusedBorder2,
    required this.defaultText,
    required this.hoverText,
    required this.disabledText,
    required this.focusedText,
    required this.activatedText,
    required this.goodText,
    required this.badText,
    required this.warningText,
    required this.defaultIcon,
    required this.hoverIcon,
    required this.disabledIcon,
    required this.focusedIcon,
    required this.activatedIcon,
    required this.goodIcon,
    required this.badIcon,
    required this.warningIcon,
    required this.counterDefaultText,
    required this.counterBadText,
  });

  final Color defaultFill;
  final Color hoverFill;
  final Color disabledFill;
  final Color focusedFill;
  final Color activatedFill;
  final Color goodFill;
  final Color badFill;
  final Color warningFill;
  final Color activatedBorder;
  final Color disabledBorder;
  final Color goodBorder;
  final Color badBorder;
  final Color warningBorder;
  final Color focusedBorder2;
  final Color defaultText;
  final Color hoverText;
  final Color disabledText;
  final Color focusedText;
  final Color activatedText;
  final Color goodText;
  final Color badText;
  final Color warningText;
  final Color defaultIcon;
  final Color hoverIcon;
  final Color disabledIcon;
  final Color focusedIcon;
  final Color activatedIcon;
  final Color goodIcon;
  final Color badIcon;
  final Color warningIcon;
  final Color counterDefaultText;
  final Color counterBadText;

  @override
  TextFieldFilledColors lerp(TextFieldFilledColors? other, double t) {
    if (other is! TextFieldFilledColors) {
      return this;
    }

    return TextFieldFilledColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      disabledFill: Color.lerp(disabledFill, other.disabledFill, t)!,
      focusedFill: Color.lerp(focusedFill, other.focusedFill, t)!,
      activatedFill: Color.lerp(activatedFill, other.activatedFill, t)!,
      goodFill: Color.lerp(goodFill, other.goodFill, t)!,
      badFill: Color.lerp(badFill, other.badFill, t)!,
      warningFill: Color.lerp(warningFill, other.warningFill, t)!,
      disabledBorder: Color.lerp(disabledBorder, other.disabledBorder, t)!,
      activatedBorder: Color.lerp(activatedBorder, other.activatedBorder, t)!,
      goodBorder: Color.lerp(goodBorder, other.goodBorder, t)!,
      badBorder: Color.lerp(badBorder, other.badBorder, t)!,
      warningBorder: Color.lerp(warningBorder, other.warningBorder, t)!,
      focusedBorder2: Color.lerp(focusedBorder2, other.focusedBorder2, t)!,
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
      hoverText: Color.lerp(hoverText, other.hoverText, t)!,
      disabledText: Color.lerp(disabledText, other.disabledText, t)!,
      focusedText: Color.lerp(focusedText, other.focusedText, t)!,
      activatedText: Color.lerp(activatedText, other.activatedText, t)!,
      goodText: Color.lerp(goodText, other.goodText, t)!,
      badText: Color.lerp(badText, other.badText, t)!,
      warningText: Color.lerp(warningText, other.warningText, t)!,
      defaultIcon: Color.lerp(defaultIcon, other.defaultIcon, t)!,
      hoverIcon: Color.lerp(hoverIcon, other.hoverIcon, t)!,
      disabledIcon: Color.lerp(disabledIcon, other.disabledIcon, t)!,
      focusedIcon: Color.lerp(focusedIcon, other.focusedIcon, t)!,
      activatedIcon: Color.lerp(activatedIcon, other.activatedIcon, t)!,
      goodIcon: Color.lerp(goodIcon, other.goodIcon, t)!,
      badIcon: Color.lerp(badIcon, other.badIcon, t)!,
      warningIcon: Color.lerp(warningIcon, other.warningIcon, t)!,
      counterDefaultText: Color.lerp(counterDefaultText, other.counterDefaultText, t)!,
      counterBadText: Color.lerp(counterBadText, other.counterBadText, t)!,
    );
  }

  @override
  TextFieldFilledColors copyWith({
    Color? defaultFill,
    Color? hoverFill,
    Color? disabledFill,
    Color? focusedFill,
    Color? activatedFill,
    Color? goodFill,
    Color? badFill,
    Color? warningFill,
    Color? disabledBorder,
    Color? activatedBorder,
    Color? goodBorder,
    Color? badBorder,
    Color? warningBorder,
    Color? focusedBorder2,
    Color? defaultText,
    Color? hoverText,
    Color? disabledText,
    Color? focusedText,
    Color? activatedText,
    Color? goodText,
    Color? badText,
    Color? warningText,
    Color? defaultIcon,
    Color? hoverIcon,
    Color? disabledIcon,
    Color? focusedIcon,
    Color? activatedIcon,
    Color? goodIcon,
    Color? badIcon,
    Color? warningIcon,
    Color? counterDefaultText,
    Color? counterBadText,
  }) {
    return TextFieldFilledColors(
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      disabledFill: disabledFill ?? this.disabledFill,
      focusedFill: focusedFill ?? this.focusedFill,
      activatedFill: activatedFill ?? this.activatedFill,
      goodFill: goodFill ?? this.goodFill,
      badFill: badFill ?? this.badFill,
      warningFill: warningFill ?? this.warningFill,
      disabledBorder: disabledBorder ?? this.disabledBorder,
      activatedBorder: activatedBorder ?? this.activatedBorder,
      goodBorder: goodBorder ?? this.goodBorder,
      badBorder: badBorder ?? this.badBorder,
      warningBorder: warningBorder ?? this.warningBorder,
      focusedBorder2: focusedBorder2 ?? this.focusedBorder2,
      defaultText: defaultText ?? this.defaultText,
      hoverText: hoverText ?? this.hoverText,
      disabledText: disabledText ?? this.disabledText,
      focusedText: focusedText ?? this.focusedText,
      activatedText: activatedText ?? this.activatedText,
      goodText: goodText ?? this.goodText,
      badText: badText ?? this.badText,
      warningText: warningText ?? this.warningText,
      defaultIcon: defaultIcon ?? this.defaultIcon,
      hoverIcon: hoverIcon ?? this.hoverIcon,
      disabledIcon: disabledIcon ?? this.disabledIcon,
      focusedIcon: focusedIcon ?? this.focusedIcon,
      activatedIcon: activatedIcon ?? this.activatedIcon,
      goodIcon: goodIcon ?? this.goodIcon,
      badIcon: badIcon ?? this.badIcon,
      warningIcon: warningIcon ?? this.warningIcon,
      counterDefaultText: counterDefaultText ?? this.counterDefaultText,
      counterBadText: counterBadText ?? this.counterBadText,
    );
  }
}
