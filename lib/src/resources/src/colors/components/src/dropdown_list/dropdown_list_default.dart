part of '../../index.dart';

class DropdownListDefaultColors
    extends ThemeExtension<DropdownListDefaultColors> {
  DropdownListDefaultColors({
    required this.defaultFill,
    required this.defaultText,
    required this.defaultIcon,
  });
  final Color defaultFill;
  final Color defaultText;
  final Color defaultIcon;

  @override
  DropdownListDefaultColors lerp(DropdownListDefaultColors? other, double t) {
    if (other is! DropdownListDefaultColors) {
      return this;
    }

    return DropdownListDefaultColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
      defaultIcon: Color.lerp(defaultIcon, other.defaultIcon, t)!,
    );
  }

  @override
  DropdownListDefaultColors copyWith({
    Color? defaultFill,
    Color? defaultText,
    Color? defaultIcon,
  }) {
    return DropdownListDefaultColors(
      defaultFill: defaultFill ?? this.defaultFill,
      defaultText: defaultText ?? this.defaultText,
      defaultIcon: defaultIcon ?? this.defaultIcon,
    );
  }
}
