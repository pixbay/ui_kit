part of '../../index.dart';

class CollapseDefaultColors extends ThemeExtension<CollapseDefaultColors> {
  CollapseDefaultColors({
    required this.defaultText,
  });

  final Color defaultText;

  @override
  CollapseDefaultColors lerp(CollapseDefaultColors? other, double t) {
    if (other is! CollapseDefaultColors) {
      return this;
    }

    return CollapseDefaultColors(
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
    );
  }

  @override
  CollapseDefaultColors copyWith({
    Color? defaultText,
  }) {
    return CollapseDefaultColors(
      defaultText: defaultText ?? this.defaultText,
    );
  }
}
