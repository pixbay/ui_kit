part of '../../index.dart';

class AccentColors extends ThemeExtension<AccentColors> {
  AccentColors({
    required this.dodger,
    required this.purple,
    required this.red,
    required this.orange,
    required this.green,
  });

  final Color dodger;
  final Color purple;
  final Color red;
  final Color orange;
  final Color green;

  @override
  AccentColors lerp(AccentColors? other, double t) {
    if (other is! AccentColors) {
      return this;
    }

    return AccentColors(
      dodger: Color.lerp(dodger, other.dodger, t)!,
      purple: Color.lerp(purple, other.purple, t)!,
      red: Color.lerp(red, other.red, t)!,
      orange: Color.lerp(orange, other.orange, t)!,
      green: Color.lerp(green, other.green, t)!,
    );
  }

  @override
  AccentColors copyWith({
    Color? dodger,
    Color? purple,
    Color? red,
    Color? orange,
    Color? green,
  }) {
    return AccentColors(
      dodger: dodger ?? this.dodger,
      purple: purple ?? this.purple,
      red: red ?? this.red,
      orange: orange ?? this.orange,
      green: green ?? this.green,
    );
  }
}
