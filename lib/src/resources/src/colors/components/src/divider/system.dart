part of '../../index.dart';

class DividerSystemColors extends ThemeExtension<DividerSystemColors> {
  DividerSystemColors({
    required this.good,
    required this.bad,
    required this.warning,
    required this.info,
  });

  final Color good;
  final Color bad;
  final Color warning;
  final Color info;

  @override
  DividerSystemColors lerp(DividerSystemColors? other, double t) {
    if (other is! DividerSystemColors) {
      return this;
    }

    return DividerSystemColors(
      good: Color.lerp(good, other.good, t)!,
      bad: Color.lerp(bad, other.bad, t)!,
      warning: Color.lerp(warning, other.warning, t)!,
      info: Color.lerp(info, other.info, t)!,
    );
  }

  @override
  DividerSystemColors copyWith({
    Color? good,
    Color? bad,
    Color? warning,
    Color? info,
  }) {
    return DividerSystemColors(
      good: good ?? this.good,
      bad: bad ?? this.bad,
      warning: warning ?? this.warning,
      info: info ?? this.info,
    );
  }
}
