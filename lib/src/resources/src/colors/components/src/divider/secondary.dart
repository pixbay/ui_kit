part of '../../index.dart';

class DividerSecondaryColors extends ThemeExtension<DividerSecondaryColors> {
  DividerSecondaryColors({
    required this.secondary70,
    required this.secondary50,
    required this.secondary30,
  });

  final Color secondary70;
  final Color secondary50;
  final Color secondary30;

  @override
  DividerSecondaryColors lerp(DividerSecondaryColors? other, double t) {
    if (other is! DividerSecondaryColors) {
      return this;
    }

    return DividerSecondaryColors(
      secondary70: Color.lerp(secondary70, other.secondary70, t)!,
      secondary50: Color.lerp(secondary50, other.secondary50, t)!,
      secondary30: Color.lerp(secondary30, other.secondary30, t)!,
    );
  }

  @override
  DividerSecondaryColors copyWith({
    Color? secondary70,
    Color? secondary50,
    Color? secondary30,
  }) {
    return DividerSecondaryColors(
      secondary70: secondary70 ?? this.secondary70,
      secondary50: secondary50 ?? this.secondary50,
      secondary30: secondary30 ?? this.secondary30,
    );
  }
}
