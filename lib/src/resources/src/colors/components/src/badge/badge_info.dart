part of '../../index.dart';

class BadgeInfoColors extends ThemeExtension<BadgeInfoColors> {
  BadgeInfoColors({
    required this.filledFill,
    required this.strokedFill,
    required this.strokedBorder,
    required this.filledText,
    required this.strokedText,
    required this.filledIcon,
    required this.strokedIcon,
  });

  final Color filledFill;
  final Color strokedFill;
  final Color strokedBorder;
  final Color filledText;
  final Color strokedText;
  final Color filledIcon;
  final Color strokedIcon;

  @override
  BadgeInfoColors lerp(BadgeInfoColors? other, double t) {
    if (other is! BadgeInfoColors) {
      return this;
    }

    return BadgeInfoColors(
      filledFill: Color.lerp(filledFill, other.filledFill, t)!,
      strokedFill: Color.lerp(strokedFill, other.strokedFill, t)!,
      strokedBorder: Color.lerp(strokedBorder, other.strokedBorder, t)!,
      filledText: Color.lerp(filledText, other.filledText, t)!,
      strokedText: Color.lerp(strokedText, other.strokedText, t)!,
      filledIcon: Color.lerp(filledIcon, other.filledIcon, t)!,
      strokedIcon: Color.lerp(strokedIcon, other.strokedIcon, t)!,
    );
  }

  @override
  BadgeInfoColors copyWith({
    Color? filledFill,
    Color? strokedFill,
    Color? strokedBorder,
    Color? filledText,
    Color? strokedText,
    Color? filledIcon,
    Color? strokedIcon,
  }) {
    return BadgeInfoColors(
      filledFill: filledFill ?? this.filledFill,
      strokedFill: strokedFill ?? this.strokedFill,
      strokedBorder: strokedBorder ?? this.strokedBorder,
      filledText: filledText ?? this.filledText,
      strokedText: strokedText ?? this.strokedText,
      filledIcon: filledIcon ?? this.filledIcon,
      strokedIcon: strokedIcon ?? this.strokedIcon,
    );
  }
}
