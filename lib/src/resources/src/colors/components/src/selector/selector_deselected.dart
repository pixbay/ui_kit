part of '../../index.dart';

class SelectorDeselectedColors
    extends ThemeExtension<SelectorDeselectedColors> {
  SelectorDeselectedColors({
    required this.defaultFill,
    required this.hoverFill,
    required this.pressedFill,
    required this.disabledFill,
    required this.focusedFill,
    required this.goodFill,
    required this.badFill,
    required this.defaultBorder,
    required this.hoverBorder,
    required this.pressedBorder,
    required this.disabledBorder,
    required this.focusedBorder,
    required this.goodBorder,
    required this.badBorder,
    required this.focusedBorder2,
  });

  final Color defaultFill;
  final Color hoverFill;
  final Color pressedFill;
  final Color disabledFill;
  final Color focusedFill;
  final Color goodFill;
  final Color badFill;
  final Color defaultBorder;
  final Color hoverBorder;
  final Color pressedBorder;
  final Color disabledBorder;
  final Color focusedBorder;
  final Color goodBorder;
  final Color badBorder;
  final Color focusedBorder2;

  @override
  SelectorDeselectedColors lerp(SelectorDeselectedColors? other, double t) {
    if (other is! SelectorDeselectedColors) {
      return this;
    }

    return SelectorDeselectedColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      pressedFill: Color.lerp(pressedFill, other.pressedFill, t)!,
      disabledFill: Color.lerp(disabledFill, other.disabledFill, t)!,
      focusedFill: Color.lerp(focusedFill, other.focusedFill, t)!,
      goodFill: Color.lerp(goodFill, other.goodFill, t)!,
      badFill: Color.lerp(badFill, other.badFill, t)!,
      defaultBorder: Color.lerp(defaultBorder, other.defaultBorder, t)!,
      hoverBorder: Color.lerp(hoverBorder, other.hoverBorder, t)!,
      pressedBorder: Color.lerp(pressedBorder, other.pressedBorder, t)!,
      disabledBorder: Color.lerp(disabledBorder, other.disabledBorder, t)!,
      focusedBorder: Color.lerp(focusedBorder, other.focusedBorder, t)!,
      goodBorder: Color.lerp(goodBorder, other.goodBorder, t)!,
      badBorder: Color.lerp(badBorder, other.badBorder, t)!,
      focusedBorder2: Color.lerp(focusedBorder2, other.focusedBorder2, t)!,
    );
  }

  @override
  SelectorDeselectedColors copyWith({
    Color? defaultFill,
    Color? hoverFill,
    Color? pressedFill,
    Color? disabledFill,
    Color? focusedFill,
    Color? goodFill,
    Color? badFill,
    Color? defaultBorder,
    Color? hoverBorder,
    Color? pressedBorder,
    Color? disabledBorder,
    Color? focusedBorder,
    Color? goodBorder,
    Color? badBorder,
    Color? focusedBorder2,
  }) {
    return SelectorDeselectedColors(
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      pressedFill: pressedFill ?? this.pressedFill,
      disabledFill: disabledFill ?? this.disabledFill,
      focusedFill: focusedFill ?? this.focusedFill,
      goodFill: goodFill ?? this.goodFill,
      badFill: badFill ?? this.badFill,
      defaultBorder: defaultBorder ?? this.defaultBorder,
      hoverBorder: hoverBorder ?? this.hoverBorder,
      pressedBorder: pressedBorder ?? this.pressedBorder,
      disabledBorder: disabledBorder ?? this.disabledBorder,
      focusedBorder: focusedBorder ?? this.focusedBorder,
      goodBorder: goodBorder ?? this.goodBorder,
      badBorder: badBorder ?? this.badBorder,
      focusedBorder2: focusedBorder2 ?? this.focusedBorder2,
    );
  }
}
