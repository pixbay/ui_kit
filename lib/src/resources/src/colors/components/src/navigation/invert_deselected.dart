part of '../../index.dart';

///
/// Где использовать
///
/// Используем в компонентах:
/// Tabs, Pagination, Breadcrumps
///
class NavigationInvertDeselectedColors
    extends ThemeExtension<NavigationInvertDeselectedColors> {
  NavigationInvertDeselectedColors({
    required this.focusedBorder2,
    required this.defaultText,
    required this.defaultIcon,
    required this.hoverIcon,
    required this.pressedIcon,
    required this.focusedIcon,
  });

  final Color focusedBorder2;
  final Color defaultText;
  final Color defaultIcon;
  final Color hoverIcon;
  final Color pressedIcon;
  final Color focusedIcon;

  @override
  NavigationInvertDeselectedColors lerp(
    NavigationInvertDeselectedColors? other,
    double t,
  ) {
    if (other is! NavigationInvertDeselectedColors) {
      return this;
    }

    return NavigationInvertDeselectedColors(
      focusedBorder2: Color.lerp(focusedBorder2, other.focusedBorder2, t)!,
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
      defaultIcon: Color.lerp(defaultIcon, other.defaultIcon, t)!,
      hoverIcon: Color.lerp(hoverIcon, other.hoverIcon, t)!,
      pressedIcon: Color.lerp(pressedIcon, other.pressedIcon, t)!,
      focusedIcon: Color.lerp(focusedIcon, other.focusedIcon, t)!,
    );
  }

  @override
  NavigationInvertDeselectedColors copyWith({
    Color? focusedBorder2,
    Color? defaultText,
    Color? defaultIcon,
    Color? hoverIcon,
    Color? pressedIcon,
    Color? focusedIcon,
  }) {
    return NavigationInvertDeselectedColors(
      focusedBorder2: focusedBorder2 ?? this.focusedBorder2,
      defaultText: defaultText ?? this.defaultText,
      defaultIcon: defaultIcon ?? this.defaultIcon,
      hoverIcon: hoverIcon ?? this.hoverIcon,
      pressedIcon: pressedIcon ?? this.pressedIcon,
      focusedIcon: focusedIcon ?? this.focusedIcon,
    );
  }
}
