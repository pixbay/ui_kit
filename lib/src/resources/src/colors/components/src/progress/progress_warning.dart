part of '../../index.dart';

class ProgressWarningColors extends ThemeExtension<ProgressWarningColors> {
  ProgressWarningColors({
    required this.fill,
    required this.text,
    required this.icon,
  });

  final Color fill;
  final Color text;
  final Color icon;

  @override
  ProgressWarningColors lerp(ProgressWarningColors? other, double t) {
    if (other is! ProgressWarningColors) {
      return this;
    }

    return ProgressWarningColors(
      fill: Color.lerp(fill, other.fill, t)!,
      text: Color.lerp(text, other.text, t)!,
      icon: Color.lerp(icon, other.icon, t)!,
    );
  }

  @override
  ProgressWarningColors copyWith({
    Color? fill,
    Color? text,
    Color? icon,
  }) {
    return ProgressWarningColors(
      fill: fill ?? this.fill,
      text: text ?? this.text,
      icon: icon ?? this.icon,
    );
  }
}
