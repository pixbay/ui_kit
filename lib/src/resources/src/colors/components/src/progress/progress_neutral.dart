part of '../../index.dart';

class ProgressNeutralColors extends ThemeExtension<ProgressNeutralColors> {
  ProgressNeutralColors({
    required this.fill,
    required this.text,
    required this.icon,
  });

  final Color fill;
  final Color text;
  final Color icon;

  @override
  ProgressNeutralColors lerp(ProgressNeutralColors? other, double t) {
    if (other is! ProgressNeutralColors) {
      return this;
    }

    return ProgressNeutralColors(
      fill: Color.lerp(fill, other.fill, t)!,
      text: Color.lerp(text, other.text, t)!,
      icon: Color.lerp(icon, other.icon, t)!,
    );
  }

  @override
  ProgressNeutralColors copyWith({
    Color? fill,
    Color? text,
    Color? icon,
  }) {
    return ProgressNeutralColors(
      fill: fill ?? this.fill,
      text: text ?? this.text,
      icon: icon ?? this.icon,
    );
  }
}
