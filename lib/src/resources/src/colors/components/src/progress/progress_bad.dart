part of '../../index.dart';

class ProgressBadColors extends ThemeExtension<ProgressBadColors> {
  ProgressBadColors({
    required this.fill,
    required this.text,
    required this.icon,
  });

  final Color fill;
  final Color text;
  final Color icon;

  @override
  ProgressBadColors lerp(ProgressBadColors? other, double t) {
    if (other is! ProgressBadColors) {
      return this;
    }

    return ProgressBadColors(
      fill: Color.lerp(fill, other.fill, t)!,
      text: Color.lerp(text, other.text, t)!,
      icon: Color.lerp(icon, other.icon, t)!,
    );
  }

  @override
  ProgressBadColors copyWith({
    Color? fill,
    Color? text,
    Color? icon,
  }) {
    return ProgressBadColors(
      fill: fill ?? this.fill,
      text: text ?? this.text,
      icon: icon ?? this.icon,
    );
  }
}
