part of '../../index.dart';

class ProgressDefaultColors extends ThemeExtension<ProgressDefaultColors> {
  ProgressDefaultColors({
    required this.fill,
    required this.text,
    required this.icon,
  });

  final Color fill;
  final Color text;
  final Color icon;

  @override
  ProgressDefaultColors lerp(ProgressDefaultColors? other, double t) {
    if (other is! ProgressDefaultColors) {
      return this;
    }

    return ProgressDefaultColors(
      fill: Color.lerp(fill, other.fill, t)!,
      text: Color.lerp(text, other.text, t)!,
      icon: Color.lerp(icon, other.icon, t)!,
    );
  }

  @override
  ProgressDefaultColors copyWith({
    Color? fill,
    Color? text,
    Color? icon,
  }) {
    return ProgressDefaultColors(
      fill: fill ?? this.fill,
      text: text ?? this.text,
      icon: icon ?? this.icon,
    );
  }
}
