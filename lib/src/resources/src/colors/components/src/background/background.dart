part of '../../index.dart';

///
/// Где использовать
///
/// Третий информационный слой.
/// Некликабельная область заднего плана интерфейса.
///
class BackgroundColors extends ThemeExtension<BackgroundColors> {
  BackgroundColors({
    required this.white,
    required this.gray,
  });

  final Color white;
  final Color gray;

  @override
  BackgroundColors lerp(BackgroundColors? other, double t) {
    if (other is! BackgroundColors) {
      return this;
    }

    return BackgroundColors(
      white: Color.lerp(white, other.white, t)!,
      gray: Color.lerp(gray, other.gray, t)!,
    );
  }

  @override
  BackgroundColors copyWith({
    Color? white,
    Color? gray,
  }) {
    return BackgroundColors(
      white: white ?? this.white,
      gray: gray ?? this.gray,
    );
  }
}
