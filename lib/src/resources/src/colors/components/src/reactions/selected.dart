part of '../../index.dart';

class ReactionsSelectedColors extends ThemeExtension<ReactionsSelectedColors> {
  ReactionsSelectedColors({
    required this.defaultFill,
    required this.hoverFill,
    required this.defaultBorder,
    required this.hoverBorder,
    required this.defaultText,
    required this.hoverText,
  });

  final Color defaultFill;
  final Color hoverFill;
  final Color defaultBorder;
  final Color hoverBorder;
  final Color defaultText;
  final Color hoverText;

  @override
  ReactionsSelectedColors lerp(ReactionsSelectedColors? other, double t) {
    if (other is! ReactionsSelectedColors) {
      return this;
    }

    return ReactionsSelectedColors(
      defaultFill: Color.lerp(defaultFill, other.defaultFill, t)!,
      hoverFill: Color.lerp(hoverFill, other.hoverFill, t)!,
      defaultBorder: Color.lerp(defaultBorder, other.defaultBorder, t)!,
      hoverBorder: Color.lerp(hoverBorder, other.hoverBorder, t)!,
      defaultText: Color.lerp(defaultText, other.defaultText, t)!,
      hoverText: Color.lerp(hoverText, other.hoverText, t)!,
    );
  }

  @override
  ReactionsSelectedColors copyWith({
    Color? defaultFill,
    Color? hoverFill,
    Color? defaultBorder,
    Color? hoverBorder,
    Color? defaultText,
    Color? hoverText,
  }) {
    return ReactionsSelectedColors(
      defaultFill: defaultFill ?? this.defaultFill,
      hoverFill: hoverFill ?? this.hoverFill,
      defaultBorder: defaultBorder ?? this.defaultBorder,
      hoverBorder: hoverBorder ?? this.hoverBorder,
      defaultText: defaultText ?? this.defaultText,
      hoverText: hoverText ?? this.hoverText,
    );
  }
}
