part of '../../../index.dart';

class DefaultLightBaseTokenColors {
  static const primary100 = Color(0xFF1A47BB);
  static const primary90 = Color(0xFF2A5CDD);
  static const primary80 = Color(0xFF3D73FF);
  static const primary70 = Color(0xFF5F8CFF);
  static const primary60 = Color(0xFF81A4FF);
  static const primary50 = Color(0xFFA3BDFF);
  static const primary40 = Color(0xFFC5D5FF);
  static const primary30 = Color(0xFFD6E2FF);
  static const primary20 = Color(0xFFE7EEFF);
  static const primary10 = Color(0xFFF8FAFF);

  static const secondary100 = Color(0xFF1A1C23);
  static const secondary80 = Color(0xFF373C4A);
  static const secondary70 = Color(0xFF5D6477);
  static const secondary60 = Color(0xFF868EA4);
  static const secondary50 = Color(0xFF9CA4BB);
  static const secondary40 = Color(0xFFB2BBD2);
  static const secondary30 = Color(0xFFD6DCED);
  static const secondary20 = Color(0xFFE6EDFA);
  static const secondary10 = Color(0xFFF5F8FF);
  static const secondary0 = Color(0xFFFFFFFF);

  static const accentBlue90 = Color(0xFF0970BB);
  static const accentBlue80 = Color(0xFF178ADD);
  static const accentBlue70 = Color(0xFF29A5FF);
  static const accentBlue60 = Color(0xFF59B9FF);
  static const accentBlue50 = Color(0xFF7BC7FF);
  static const accentBlue40 = Color(0xFFA0D7FF);
  static const accentBlue30 = Color(0xFFC2E5FF);
  static const accentBlue20 = Color(0xFFB6CADA);

  static const accentPurple90 = Color(0xFF702EA4);
  static const accentPurple80 = Color(0xFF8B40C6);
  static const accentPurple70 = Color(0xFFA755E8);
  static const accentPurple60 = Color(0xFFBC69FF);
  static const accentPurple50 = Color(0xFFCB8AFF);
  static const accentPurple40 = Color(0xFFDAACFF);
  static const accentPurple30 = Color(0xFFE9CEFF);
  static const accentPurple20 = Color(0xFFF8F0FF);

  static const accentRed90 = Color(0xFFAA2020);
  static const accentRed80 = Color(0xFFCC3030);
  static const accentRed70 = Color(0xFFEE4444);
  static const accentRed60 = Color(0xFFFD7171);
  static const accentRed50 = Color(0xFFFF9B9B);
  static const accentRed40 = Color(0xFFFFC4C4);
  static const accentRed30 = Color(0xFFFFD5D5);
  static const accentRed20 = Color(0xFFFFEDED);

  static const accentOrange90 = Color(0xFFBB3402);
  static const accentOrange80 = Color(0xFFDD4610);
  static const accentOrange70 = Color(0xFFFF5C21);
  static const accentOrange60 = Color(0xFFFF8153);
  static const accentOrange50 = Color(0xFFFFA585);
  static const accentOrange40 = Color(0xFFFFC0A9);
  static const accentOrange30 = Color(0xFFFFD9CB);
  static const accentOrange20 = Color(0xFFFFEEE9);

  static const accentYellow90 = Color(0xFFBB8900);
  static const accentYellow80 = Color(0xFFDDA200);
  static const accentYellow70 = Color(0xFFFFBB00);
  static const accentYellow60 = Color(0xFFFFC933);
  static const accentYellow50 = Color(0xFFFFD666);
  static const accentYellow40 = Color(0xFFFFE499);
  static const accentYellow30 = Color(0xFFFFEDBB);
  static const accentYellow20 = Color(0xFFFFF6DD);

  static const accentGreen90 = Color(0xFF328428);
  static const accentGreen80 = Color(0xFF46A63A);
  static const accentGreen70 = Color(0xFF5CC84F);
  static const accentGreen60 = Color(0xFF7BE36E);
  static const accentGreen50 = Color(0xFF9EF094);
  static const accentGreen40 = Color(0xFFBEFFB7);
  static const accentGreen30 = Color(0xFFD9FFD4);
  static const accentGreen20 = Color(0xFFF3FFF2);

  static const systemBad90 = Color(0xFFA20F0F);
  static const systemBad70 = Color(0xFFE62F2F);
  static const systemBad40 = Color(0xFFFF9797);
  static const systemBad20 = Color(0xFFFFECEC);

  static const systemWarning90 = Color(0xFFBB3402);
  static const systemWarning70 = Color(0xFFFF5C21);
  static const systemWarning40 = Color(0xFFFFC0A9);
  static const systemWarning20 = Color(0xFFFFEEE9);

  static const systemGood90 = Color(0xFF146F14);
  static const systemGood70 = Color(0xFF34B334);
  static const systemGood40 = Color(0xFFB7EDB7);
  static const systemGood20 = Color(0xFFF0FFF0);

  static const systemInfo90 = Color(0xFF005AAD);
  static const systemInfo70 = Color(0xFF148EFF);
  static const systemInfo40 = Color(0xFF93CBFF);
  static const systemInfo20 = Color(0xFFE7F3FF);

  static const systemNeutral90 = Color(0xFF282C37);
  static const systemNeutral70 = Color(0xFF5D6477);
  static const systemNeutral40 = Color(0xFFB2BBD2);
  static const systemNeutral20 = Color(0xFFE6EDFA);

  static const gradientBlueWhite = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: <Color>[
      Color(0xFF4081FF),
      Color(0xFF7098FF),
    ],
  );

  static const gradientOrangeWhite = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: <Color>[
      Color(0xFFFF6022),
      Color(0xFFFD9B27),
    ],
  );

  static const gradientGreenWhite = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: <Color>[
      Color(0xFF5BC750),
      Color(0xFF7EE431),
    ],
  );

  static const gradientWhiteUp = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: <Color>[
      Color(0xFFFFFFFF),
      Color(0x00FFFFFF),
    ],
  );

  static const gradientWhiteDown = LinearGradient(
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter,
    colors: <Color>[
      Color(0x00FFFFFF),
      Color(0xFFFFFFFF),
    ],
  );
}

class DefaultShadows {
  static const baseShadow = BoxShadow(
    color: Color.fromRGBO(178, 93, 108, 0.25),
    offset: Offset(0.0, 2.5),
    blurRadius: 2.0,
    spreadRadius: 0.0,
  );
}
