import 'package:flutter/material.dart';
import 'package:ui_kit/src/resources/index.dart';

class TextEditFormField extends StatefulWidget {
  TextEditFormField({
    super.key,
    this.isError = false,
    this.hintText,
    this.initialValue,
    this.errorText,
    this.isVisible = true,
    this.isSecure = false,
    this.enabled = true,
    TextEditingController? controller,
    this.validator,
    this.focusNode,
    this.hintStyle,
    this.labelStyle,
    this.filled = true,
    this.isBorder = true,
    this.textInputAction = TextInputAction.done,
    this.onChanged,
    this.onEditingComplete,
    this.onFieldSubmitted,
    this.maxLength,
    this.autofocus = false,
    this.onTap,
    this.onTapOutside,
    this.prefixIcon,
    this.suffixIcon,
  }) : controller = controller ?? TextEditingController(text: initialValue ?? '');

  final GestureTapCallback? onTap;

  final bool isError;
  final String? hintText;
  final String? initialValue;
  final String? errorText;
  final bool isVisible;
  final bool autofocus;
  final bool isSecure;
  final bool enabled;
  final TextEditingController? controller;
  final FormFieldValidator<String>? validator;
  final FocusNode? focusNode;
  final TextStyle? hintStyle;
  final TextStyle? labelStyle;
  final bool filled;
  final bool isBorder;
  final TextInputAction textInputAction;
  final ValueChanged<String>? onChanged;
  final VoidCallback? onEditingComplete;
  final ValueChanged<String>? onFieldSubmitted;
  final int? maxLength;
  final TapRegionCallback? onTapOutside;
  final Widget? prefixIcon;
  final Widget? suffixIcon;

  @override
  State<TextEditFormField> createState() => _TextEditFormFieldState();
}

class _TextEditFormFieldState extends State<TextEditFormField> {
  bool _isVisible = true;

  @override
  void initState() {
    super.initState();

    _isVisible = widget.isVisible;
  }

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    final textFieldFilled = theme.extension<TextFieldFilledColors>()!;
    final textFieldEmptyColors = theme.extension<TextFieldEmptyColors>()!;
    final textSystem = theme.extension<TextSystemColors>()!;
    final buttonGhostColors = theme.extension<ButtonGhostColors>()!;

    final transparentBorder = OutlineInputBorder(
      borderSide: const BorderSide(
        color: Colors.transparent,
        width: 0.0,
      ),
      borderRadius: BorderRadius.circular(Radiuses.radius2XS),
    );

    final border = OutlineInputBorder(
      borderSide: BorderSide(
        color: textFieldFilled.activatedBorder,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(Radiuses.radius2XS),
    );

    final enabledBorder = OutlineInputBorder(
      borderSide: BorderSide(
        color: textFieldFilled.activatedBorder,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(Radiuses.radius2XS),
    );

    final disabledBorder = OutlineInputBorder(
      borderSide: BorderSide(
        color: textFieldFilled.disabledBorder,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(Radiuses.radius2XS),
    );

    final focusedBorder = OutlineInputBorder(
      borderSide: BorderSide(
        color: textFieldFilled.activatedBorder,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(Radiuses.radius2XS),
    );

    final errorBorder = OutlineInputBorder(
      borderSide: BorderSide(
        color: textFieldFilled.badBorder,
        width: 1,
      ),
      borderRadius: BorderRadius.circular(Radiuses.radius2XS),
    ); //MaterialTextSelectionControls();

    final textFormField = TextFormField(
      onTap: widget.onTap,
      readOnly: false,
      enableSuggestions: false,
      enableInteractiveSelection: true,
      autofocus: widget.autofocus,
      enabled: widget.enabled,
      maxLength: widget.maxLength,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      onTapOutside: widget.onTapOutside,
      onEditingComplete: widget.onEditingComplete,
      onFieldSubmitted: widget.onFieldSubmitted,
      onChanged: widget.onChanged,
      controller: widget.controller,
      keyboardType: TextInputType.text,
      textInputAction: widget.textInputAction,
      validator: widget.validator,
      obscureText: !(!widget.isSecure || _isVisible),
      focusNode: widget.focusNode,
      style: theme.textTheme.headlineLarge?.copyWith(
        color: textFieldFilled.badText,
      ),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.symmetric(
          vertical: 10.0,
          horizontal: widget.isBorder ? 14.0 : 0.0,
        ),
        prefixIcon: widget.prefixIcon,
        suffixIcon: widget.suffixIcon ??
            (widget.isSecure
                ? IconButton(
                    onPressed: () {
                      setState(
                        () {
                          _isVisible = !_isVisible;
                        },
                      );
                    },
                    icon: IconWidget.size2XM(
                      color: buttonGhostColors.defaultIcon,
                      type: _isVisible ? IconType.eyeConturOpen : IconType.eyeConturClose,
                    ),
                  )
                : null),
        hintText: widget.hintText,
        errorText: widget.errorText,
        hintStyle: widget.hintStyle ??
            theme.textTheme.bodyMedium?.copyWith(
              color: textFieldEmptyColors.activatedText,
            ),
        labelStyle: widget.labelStyle ??
            theme.textTheme.bodyMedium?.copyWith(
              color: textFieldEmptyColors.activatedText,
            ),
        errorStyle: theme.textTheme.bodySmall?.copyWith(
          color: textSystem.bad,
        ),
        fillColor: textFieldFilled.activatedFill,
        errorBorder: errorBorder,
        focusedBorder: widget.isError
            ? errorBorder
            : widget.isBorder
                ? focusedBorder
                : transparentBorder,
        focusedErrorBorder: errorBorder,
        disabledBorder: widget.isError
            ? errorBorder
            : widget.isBorder
                ? disabledBorder
                : transparentBorder,
        enabledBorder: widget.isError ? errorBorder : enabledBorder,
        border: widget.isError
            ? errorBorder
            : widget.isBorder
                ? border
                : transparentBorder,
        enabled: widget.enabled,
        filled: widget.filled,
      ),
    );

    return textFormField;
  }
}
