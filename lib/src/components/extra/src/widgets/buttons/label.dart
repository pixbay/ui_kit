import 'package:flutter/material.dart';
import 'package:ui_kit/src/resources/index.dart';

class LabelButtonWidget extends StatelessWidget {
  const LabelButtonWidget({
    super.key,
    required this.onTap,
    required this.label,
  });

  final GestureTapCallback onTap;
  final String label;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textPrimaryColors = theme.extension<TextPrimaryColors>()!;
    return GestureDetector(
      onTap: onTap,
      child: Padding(
        padding: const EdgeInsets.all(Spacing.spacing2XS),
        child: Text(
          label,
          style: textTheme.bodyLarge!.copyWith(
            color: textPrimaryColors.primary60,
          ),
        ),
      ),
    );
  }
}
