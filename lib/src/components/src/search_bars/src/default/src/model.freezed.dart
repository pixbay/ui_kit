// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$SearchBarModel {
  String get text => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $SearchBarModelCopyWith<SearchBarModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $SearchBarModelCopyWith<$Res> {
  factory $SearchBarModelCopyWith(
          SearchBarModel value, $Res Function(SearchBarModel) then) =
      _$SearchBarModelCopyWithImpl<$Res, SearchBarModel>;
  @useResult
  $Res call({String text});
}

/// @nodoc
class _$SearchBarModelCopyWithImpl<$Res, $Val extends SearchBarModel>
    implements $SearchBarModelCopyWith<$Res> {
  _$SearchBarModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? text = null,
  }) {
    return _then(_value.copyWith(
      text: null == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$SearchBarModelImplCopyWith<$Res>
    implements $SearchBarModelCopyWith<$Res> {
  factory _$$SearchBarModelImplCopyWith(_$SearchBarModelImpl value,
          $Res Function(_$SearchBarModelImpl) then) =
      __$$SearchBarModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String text});
}

/// @nodoc
class __$$SearchBarModelImplCopyWithImpl<$Res>
    extends _$SearchBarModelCopyWithImpl<$Res, _$SearchBarModelImpl>
    implements _$$SearchBarModelImplCopyWith<$Res> {
  __$$SearchBarModelImplCopyWithImpl(
      _$SearchBarModelImpl _value, $Res Function(_$SearchBarModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? text = null,
  }) {
    return _then(_$SearchBarModelImpl(
      text: null == text
          ? _value.text
          : text // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$SearchBarModelImpl implements _SearchBarModel {
  const _$SearchBarModelImpl({required this.text});

  @override
  final String text;

  @override
  String toString() {
    return 'SearchBarModel(text: $text)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$SearchBarModelImpl &&
            (identical(other.text, text) || other.text == text));
  }

  @override
  int get hashCode => Object.hash(runtimeType, text);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$SearchBarModelImplCopyWith<_$SearchBarModelImpl> get copyWith =>
      __$$SearchBarModelImplCopyWithImpl<_$SearchBarModelImpl>(
          this, _$identity);
}

abstract class _SearchBarModel implements SearchBarModel {
  const factory _SearchBarModel({required final String text}) =
      _$SearchBarModelImpl;

  @override
  String get text;
  @override
  @JsonKey(ignore: true)
  _$$SearchBarModelImplCopyWith<_$SearchBarModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
