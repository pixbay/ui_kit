import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class SearchBarModel with _$SearchBarModel {
  const factory SearchBarModel({
    required String text,
  }) = _SearchBarModel;
}
