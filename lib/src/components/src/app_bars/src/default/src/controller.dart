part of '../index.dart';

class APDefaultAppBarController extends ValueNotifier<APDefaultAppBarModel> {
  APDefaultAppBarController({
    required model,
  }) : super(model);

  set title(String v) {
    value = value.copyWith(
      title: v,
    );
  }
}
