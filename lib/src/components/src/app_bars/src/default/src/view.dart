part of '../index.dart';

class DefaultAppBar extends AAppBar {
  DefaultAppBar({
    this.rightChild,
    this.centerChild,
    this.leftChild,
    super.onBack,
    title = '',
    super.key,
  }) : controller = APDefaultAppBarController(
          model: APDefaultAppBarModel(
            title: title,
          ),
        );

  final APDefaultAppBarController controller;
  final Widget? leftChild;
  final Widget? centerChild;
  final Widget? rightChild;

  @override
  State<DefaultAppBar> createState() => _APDefaultAppBarState();
}

class _APDefaultAppBarState extends AAPState<DefaultAppBar> {
  @override
  Widget buildContent(BuildContext context) {
    return ValueListenableBuilder<APDefaultAppBarModel>(
      valueListenable: widget.controller,
      builder: (
        BuildContext context,
        APDefaultAppBarModel value,
        Widget? child,
      ) {
        return Row(
          children: [
            const SizedBox(
              width: Spacing.spacing3XS,
            ),
            Expanded(
              child: widget.centerChild ??
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      TitleAppBar(text: widget.controller.value.title),
                    ],
                  ),
            ),
            const SizedBox(
              width: Spacing.spacing3XS,
            ),
          ],
        );
      },
    );
  }
}
