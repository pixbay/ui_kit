import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class APDefaultAppBarModel with _$APDefaultAppBarModel {
  const factory APDefaultAppBarModel({
    required String title,
  }) = _APDefaultAppBarModel;
}
