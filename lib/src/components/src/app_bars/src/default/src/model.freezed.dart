// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$APDefaultAppBarModel {
  String get title => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $APDefaultAppBarModelCopyWith<APDefaultAppBarModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $APDefaultAppBarModelCopyWith<$Res> {
  factory $APDefaultAppBarModelCopyWith(APDefaultAppBarModel value,
          $Res Function(APDefaultAppBarModel) then) =
      _$APDefaultAppBarModelCopyWithImpl<$Res, APDefaultAppBarModel>;
  @useResult
  $Res call({String title});
}

/// @nodoc
class _$APDefaultAppBarModelCopyWithImpl<$Res,
        $Val extends APDefaultAppBarModel>
    implements $APDefaultAppBarModelCopyWith<$Res> {
  _$APDefaultAppBarModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
  }) {
    return _then(_value.copyWith(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$APDefaultAppBarModelImplCopyWith<$Res>
    implements $APDefaultAppBarModelCopyWith<$Res> {
  factory _$$APDefaultAppBarModelImplCopyWith(_$APDefaultAppBarModelImpl value,
          $Res Function(_$APDefaultAppBarModelImpl) then) =
      __$$APDefaultAppBarModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String title});
}

/// @nodoc
class __$$APDefaultAppBarModelImplCopyWithImpl<$Res>
    extends _$APDefaultAppBarModelCopyWithImpl<$Res, _$APDefaultAppBarModelImpl>
    implements _$$APDefaultAppBarModelImplCopyWith<$Res> {
  __$$APDefaultAppBarModelImplCopyWithImpl(_$APDefaultAppBarModelImpl _value,
      $Res Function(_$APDefaultAppBarModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = null,
  }) {
    return _then(_$APDefaultAppBarModelImpl(
      title: null == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$APDefaultAppBarModelImpl implements _APDefaultAppBarModel {
  const _$APDefaultAppBarModelImpl({required this.title});

  @override
  final String title;

  @override
  String toString() {
    return 'APDefaultAppBarModel(title: $title)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$APDefaultAppBarModelImpl &&
            (identical(other.title, title) || other.title == title));
  }

  @override
  int get hashCode => Object.hash(runtimeType, title);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$APDefaultAppBarModelImplCopyWith<_$APDefaultAppBarModelImpl>
      get copyWith =>
          __$$APDefaultAppBarModelImplCopyWithImpl<_$APDefaultAppBarModelImpl>(
              this, _$identity);
}

abstract class _APDefaultAppBarModel implements APDefaultAppBarModel {
  const factory _APDefaultAppBarModel({required final String title}) =
      _$APDefaultAppBarModelImpl;

  @override
  String get title;
  @override
  @JsonKey(ignore: true)
  _$$APDefaultAppBarModelImplCopyWith<_$APDefaultAppBarModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
