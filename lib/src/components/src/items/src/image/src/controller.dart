part of '../index.dart';

class ImageItemController extends ValueNotifier<ImageItemModel> {
  ImageItemController({
    required ImageItemModel model,
  }) : super(
          model,
        );

  set likes(int v) {
    value = value.copyWith(likes: v);
  }

  set views(int v) {
    value = value.copyWith(views: v);
  }
}
