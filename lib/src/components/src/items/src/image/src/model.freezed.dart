// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$ImageItemModel {
  String get imagePath => throw _privateConstructorUsedError;
  int get likes => throw _privateConstructorUsedError;
  int get views => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $ImageItemModelCopyWith<ImageItemModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ImageItemModelCopyWith<$Res> {
  factory $ImageItemModelCopyWith(
          ImageItemModel value, $Res Function(ImageItemModel) then) =
      _$ImageItemModelCopyWithImpl<$Res, ImageItemModel>;
  @useResult
  $Res call({String imagePath, int likes, int views});
}

/// @nodoc
class _$ImageItemModelCopyWithImpl<$Res, $Val extends ImageItemModel>
    implements $ImageItemModelCopyWith<$Res> {
  _$ImageItemModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? imagePath = null,
    Object? likes = null,
    Object? views = null,
  }) {
    return _then(_value.copyWith(
      imagePath: null == imagePath
          ? _value.imagePath
          : imagePath // ignore: cast_nullable_to_non_nullable
              as String,
      likes: null == likes
          ? _value.likes
          : likes // ignore: cast_nullable_to_non_nullable
              as int,
      views: null == views
          ? _value.views
          : views // ignore: cast_nullable_to_non_nullable
              as int,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$ImageItemModelImplCopyWith<$Res>
    implements $ImageItemModelCopyWith<$Res> {
  factory _$$ImageItemModelImplCopyWith(_$ImageItemModelImpl value,
          $Res Function(_$ImageItemModelImpl) then) =
      __$$ImageItemModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String imagePath, int likes, int views});
}

/// @nodoc
class __$$ImageItemModelImplCopyWithImpl<$Res>
    extends _$ImageItemModelCopyWithImpl<$Res, _$ImageItemModelImpl>
    implements _$$ImageItemModelImplCopyWith<$Res> {
  __$$ImageItemModelImplCopyWithImpl(
      _$ImageItemModelImpl _value, $Res Function(_$ImageItemModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? imagePath = null,
    Object? likes = null,
    Object? views = null,
  }) {
    return _then(_$ImageItemModelImpl(
      imagePath: null == imagePath
          ? _value.imagePath
          : imagePath // ignore: cast_nullable_to_non_nullable
              as String,
      likes: null == likes
          ? _value.likes
          : likes // ignore: cast_nullable_to_non_nullable
              as int,
      views: null == views
          ? _value.views
          : views // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$ImageItemModelImpl implements _ImageItemModel {
  const _$ImageItemModelImpl(
      {required this.imagePath, required this.likes, required this.views});

  @override
  final String imagePath;
  @override
  final int likes;
  @override
  final int views;

  @override
  String toString() {
    return 'ImageItemModel(imagePath: $imagePath, likes: $likes, views: $views)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$ImageItemModelImpl &&
            (identical(other.imagePath, imagePath) ||
                other.imagePath == imagePath) &&
            (identical(other.likes, likes) || other.likes == likes) &&
            (identical(other.views, views) || other.views == views));
  }

  @override
  int get hashCode => Object.hash(runtimeType, imagePath, likes, views);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$ImageItemModelImplCopyWith<_$ImageItemModelImpl> get copyWith =>
      __$$ImageItemModelImplCopyWithImpl<_$ImageItemModelImpl>(
          this, _$identity);
}

abstract class _ImageItemModel implements ImageItemModel {
  const factory _ImageItemModel(
      {required final String imagePath,
      required final int likes,
      required final int views}) = _$ImageItemModelImpl;

  @override
  String get imagePath;
  @override
  int get likes;
  @override
  int get views;
  @override
  @JsonKey(ignore: true)
  _$$ImageItemModelImplCopyWith<_$ImageItemModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
