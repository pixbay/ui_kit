import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class ImageItemModel with _$ImageItemModel {
  const factory ImageItemModel({
    required String imagePath,
    required int likes,
    required int views,
  }) = _ImageItemModel;
}
