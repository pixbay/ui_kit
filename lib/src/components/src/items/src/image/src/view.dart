part of '../index.dart';

class ImageItemWidget extends StatelessWidget {
  ImageItemWidget({
    super.key,
    required ImageItemModel model,
    this.imageLoadType = ImageLoadType.network,
  }) : controller = ImageItemController(model: model);

  const ImageItemWidget.controller({
    super.key,
    required this.controller,
    this.imageLoadType = ImageLoadType.network,
  });

  final ImageItemController controller;
  final ImageLoadType imageLoadType;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final textTheme = theme.textTheme;

    final textSecondaryColors = theme.extension<TextSecondaryColors>()!;
    final iconSecondaryColors = theme.extension<IconSecondaryColors>()!;
    final backgroundColors = theme.extension<BackgroundColors>()!;
    final dividerSecondaryColors = theme.extension<DividerSecondaryColors>()!;

    return ValueListenableBuilder<ImageItemModel>(
      valueListenable: controller,
      builder: (
        BuildContext context,
        ImageItemModel value,
        Widget? child,
      ) {
        return DecoratedBox(
          decoration: BoxDecoration(
            color: backgroundColors.gray,
            borderRadius: const BorderRadius.all(
              Radius.circular(
                Radiuses.radius2XS,
              ),
            ),
            border: Border.all(
              color: dividerSecondaryColors.secondary30,
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(
              Spacing.spacing2XS,
            ),
            child: Column(
              children: [
                Expanded(
                  child: ImageLoad(
                    fit: BoxFit.cover,
                    path: value.imagePath,
                    imageLoadType: imageLoadType,
                  ),
                ),
                const SizedBox(
                  height: Spacing.spacing2XS,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    IconWidget.sizeS(
                      color: iconSecondaryColors.secondary70,
                      type: IconType.eyeConturOpen,
                    ),
                    const SizedBox(
                      width: Spacing.spacing3XS,
                    ),
                    Text(
                      '${value.views}',
                      style: textTheme.bodyMedium!.copyWith(
                        color: textSecondaryColors.secondary70,
                      ),
                    ),
                    const SizedBox(
                      width: Spacing.spacing2XS,
                    ),
                    IconWidget.sizeS(
                      color: iconSecondaryColors.secondary70,
                      type: IconType.favorite,
                    ),
                    const SizedBox(
                      width: Spacing.spacing3XS,
                    ),
                    Text(
                      '${value.likes}',
                      style: textTheme.bodyMedium!.copyWith(
                        color: textSecondaryColors.secondary70,
                      ),
                    ),
                    const Spacer(),
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }
}
