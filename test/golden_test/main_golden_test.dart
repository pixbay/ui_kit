import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:golden_toolkit/golden_toolkit.dart';
import 'package:ui_kit/generated/assets.dart';
import 'package:ui_kit/ui_kit.dart';

const _surfaceWidth = 800.0;

void main() async {
  await loadAppFonts();

  const defaultTheme = 'defaultTheme';

  group(
    'app_bar/',
    () {
      const path = '$defaultTheme/app_bar/';

      testGoldens('app_bar', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'DefaultAppBar()',
            _GoldenTestWrapperWidget(
              child: DefaultAppBar(title: 'PixaBay'),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.defaultTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}DefaultAppBar',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'item/',
    () {
      const path = '$defaultTheme/item/';

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'ImageItemWidget()',
            _GoldenTestWrapperWidget(
              child: SizedBox(
                width: 200.0,
                height: 220.0,
                child: ImageItemWidget(
                  imageLoadType: ImageLoadType.asset,
                  model: const ImageItemModel(
                    imagePath: Assets.imageImage1,
                    likes: 100,
                    views: 100,
                  ),
                ),
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.defaultTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}ImageItemWidget',
          autoHeight: true,
        );
      });
    },
  );

  group(
    'search_bars/default/',
    () {
      const path = '$defaultTheme/search_bars/default/';

      testGoldens('item', (tester) async {
        final builder = GoldenBuilder.column()
          ..addScenario(
            'SearchBarWidget()',
            _GoldenTestWrapperWidget(
              child: SearchBarWidget(
                cancelLabel: 'Cancel',
                model: const SearchBarModel(text: ''),
              ),
            ),
          );

        await tester.pumpWidgetBuilder(
          SingleChildScrollView(
            child: builder.build(),
          ),
          surfaceSize: const Size(_surfaceWidth, 0.0),
          wrapper: materialAppWrapper(
            theme: Themes.defaultTheme,
            platform: TargetPlatform.android,
          ),
        );

        await screenMatchesGolden(
          tester,
          '${path}SearchBarWidget',
          autoHeight: true,
        );
      });
    },
  );
}

class _GoldenTestWrapperWidget extends StatelessWidget {
  const _GoldenTestWrapperWidget({
    required this.child,
  });

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Center(
        child: child,
      ),
    );
  }
}
